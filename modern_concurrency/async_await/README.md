# I. Async / Await

<a name="table_of_contents"></a>
## Содержание:

1. [async/await syntax](#async_await)
2. [async throws](#async_throws)
3. [async computed properties](#async_computed)
4. [async let](#async_let)
5. [async var](#async_var)
6. [What calls the first async function?](#async_virus)
7. [Performance cost of calling an async function](#performance_cost)
8. [How to use continuations to convert completion handlers into async functions](#continuations_handlers)
9. [Wrap delegates with continuations](#continuations_delegates)

← [Назад к Modern Swift Concurrency](../README.md)

##

<a name="async_await"></a>
## 1. async/await syntax

Для того чтобы запустить асинхронное выполнение нужно 2 шага: пометить функцию `async`, затем вызвать ее с помощью `await`.

* Каждый `await` является потенциальной точкой приостановки. В момент вызова `await` код будет приостановлен до тех пор, пока `async`-функция не вернет результат или не выбросит ошибку;
* Swift последовательно выполнит каждый `await`, ожидая завершения предыдущего. `await` ***не*** приведет к *параллельному* выполнению нескольких задач;
* После возобновления результат будет присвоен переменной и теперь это обычные «синхронные» данные;
* Поскольку `await` является точкой приостановки, требуется, чтобы вызывающий метод сам по себе был асинхронным, без этого код не скомпилируется.

<img src="assets/async_await_flow_illustration.png" width="300">
<img src="assets/async_await_flow_illustration_2.png" width="450">

Пример:

<img src="assets/async_await_example.png" width="600">
<br><br>

В то время, пока функция приостановлена в точке await, может выполняться какой-то другой, наиболее приоритетный код, пока результат формируется в другом потоке.

> **⚠️ Даже если асинхронная функция готова вернуть результат немедленно, это не значит, что приостановки не будет. Система может решить, что в этот момент нужно выполнить какую-либо более приоритетную задачу.**

При использовании `await` формируется некий снапшот состояния среды, который будет вновь загружен и продолжен, когда асинхронная функция вернет результат.

> **⚠️ Когда выполнение кода будет возобновлено, мы можем оказаться на совсем другом потоке! Система сама распределяет ресурсы наиболее эффективным способом.**

Чтобы вернуться на главный поток (для обновления UI) нужно вызвать аналог `DispatchQueue.main` - `MainActor`, либо обозначить функцию, которую нужно выполнить на главном потоке аннотацией `@MainActor`. Этой же аннотацией можно пометить весь класс, тогда все обращения к его методам будут выполняться на главном потоке.

`await MainActor.run { … }`  -  await нужен и здесь, так как происходит переключение потоков.




<br><br><br>
<a name="async_throws"></a>
## 2. async throws

Если функция является одновременно асинхронной и выбрасывающей ошибку, она отмечается `async throws`, а ее вызов происходит через `try await`.

Порядок ключевых слов важен, менять его нельзя, Apple так сказала.

<img src="assets/async_throws.png" width="600">




<br><br><br>
<a name="async_computed"></a>
## 3. async computed properties

Асинхронными можно сделать и computed properties:

<img src="assets/async_computed_property.png" width="600">

Использование:

<img src="assets/async_computed_property_call.png" width="410">  




<br><br><br>
<a name="async_let"></a>
## 4. async let

Итак, все `await` выполняются последовательно, по очереди, словно синхронный код:

<img src="assets/async_let_await.png" width="470">  
<br><br>

А что если нам нужно запустить несколько независимых запросов одновременно, или просто не ждать пока запрос выполнится?
Для этого появился `async let`.
`async let` похож на promises в других языках, но в Swift он более глубоко интегрирован с рантаймом.

Все `async let` выполняются _асинхронно_ (возможно даже _параллельно_ - зависит от того, на какие потоки попадут задачи):

<img src="assets/async_let.png" width="480">  

В момент вызова `async let` асинхронная функция начинает выполняться немедленно (точнее, она встает в очередь на выполнение), а текущий код продолжает выполняться дальше.

Переменная `news` в этом случае будет иметь тип `async [News]`. 
Обращение к `async let`-переменной выполняется через `await`.

> **⚠️ `await` приостанавливает выполнение и ждет возвращения результата. `async let` - не ждет.**

Когда мы решим обратиться к `async let`-переменной, может быть две ситуации:
1. На момент обращения к ней асинхронная функция уже завершилась и вернула результат, тогда код будет выполнен немедленно, будто это самая обычная переменная;
2. Если результат еще не вернулся, выполнение кода будет приостановлено до тех пор, пока результат не будет получен.

<img src="assets/async_let_vs_await.png" width="650">

> **⚠️ Результаты `async let`-вызовов приходят в порядке кто первым успел. Порядок вызова не гарантирует, что результаты будут получены в том же порядке!**

В примере мы дожидаемся получения всех 3 переменных, только после чего возвращаем их:

<img src="assets/async_let.png" width="470"> 

Если нужно выполнить какую-то логику сразу для той переменной, которая пришла первой, нужно использовать [Task](../task_taskgroup/README.md).

> **⚠️ `async let` захватывает (captures) ссылки на все данные, с которыми взаимодействует.**

<img src="assets/async_let_capture.png" width="520">

Даже если `user` будет структурой, он будет захвачен, а не скопирован, поэтому при попытке его изменения далее в коде Swift выдаст ошибку `Reference to captured var 'user' in concurrently-executing code`.




<br><br><br>
<a name="async_var"></a>
## 5. async var

`async var` в Swift не появился, поскольку не понятно что делать со следующим кодом:

<img src="assets/async_var.png" width="380">

_При присваивании нового значения переменной асинхронная задача отменяется?_
_Если нет, когда результат придет, должен ли он перезаписать установленное значение?_
_Нужно обращаться к переменной через `await` после того, как мы явно установили ей значение?_

В общем, _it’s illegal_.




<br><br><br>
<a name="async_virus"></a>
## 6. What calls the first async function? (async virus)

Чтобы получить возможность вызывать асинхронную функцию мы должны пометить `async` *вызывающую* функцию. Однако, подписывание всего подряд как `async` приведет к `async`-вирусу, т.к. все вызывающие их функции так же нужно будет помечать `async` и так до тех пор, пока мы не превратим одну ошибку в 10. 

При попытке вызвать асинхронную функцию через `await` в синхронном контекcте, например, в методе `viewDidLoad()` мы получим ошибку `async call in a function that does not support concurrency`, ведь `viewDidLoad()` не асинхронный и не может быть приостановлен. 

Выхода из этой ситуации 3:
1. Метод `main()` в _консольных_ приложениях можно пометить `async`;
2. В SwiftUI у view есть специальные модификаторы `refreshable()` и `task()`, которые являются асинхронными и автоматически отменяют все подзадачи при уничтожении `view`;
3. В UIKit остается только [`Task`](../task_taskgroup/README.md) - в какой-то мере аналог `DispatchQueue.async`. Вызов асинхронной функции в другом потоке (когда нам не нужно ждать ее завершения) не приводит к приостановке текущей функции, значит все легально. Если нам все же нужно дождаться результата, то без `await` никак не обойтись, а значит этого не сделать в синхронном контексте.

<img src="assets/task_await.png" width="270">




<br><br><br>
<a name="performance_cost"></a>
## 7. Performance cost of calling an async function

Затраты производительности при вызове асинхронной функции зависит от выполняемой операции:

* Если потребуется приостановка, Swift приостановит работу функции и всех ее вызвавших, что приводит к небольшим затратам производительности, которые при **правильном** использовании являются пренебрежимо малыми.
* Если приостановка не требуется, функция продолжит выполнение так же, как и обычная (синхронная) функция.




<br><br><br>
<a name="continuations_handlers"></a>
## 8. How to use continuations to convert completion handlers into async functions

**async/await полностью доступен с iOS 13.** Но новинки в Foundation API, такие как `async URLSession.task` - только с iOS 15, для них обратной совместимости не будет. 
Так для чего же тогда использовать async/await?

По-старинке мы использовали completion handlers или delegates, которые вызывались после получения результата.
С помощью `continuation` можно конвертировать completion handler в `async`-функцию.

`continuation` - это грубо говоря тот самый снапшот, который система сохраняет, а затем загружает обратно и продолжает выполнение при работе с `async`-функциями.
И возобновлением этого снапшота можем управлять мы:  

<img src="assets/continuation_explanation.png" width="750">

Выполнение текущей функции будет приостановлено до тех пор, пока мы не возобновим замыкание методом `resume`.

Для создания `continuation` доступны следующие методы (справа тип создаваемого `continuation`):
- `withCheckedContinuation`  - `CheckedContinuation<T, Never>`
- `withCheckedThrowingContinuation`  - `CheckedContinuation<T, Error>`
- `withUnsafeContinuation`  - `UnsafeContinuation<T, Never>`
- `withUnsafeThrowingContinuation`  - `UnsafeContinuation<T, Error>`

> **⚠️ `continuation` должен возобновиться _один_ раз и только один раз. Отсутствие возобновления `continuation` означает его утечку, а значит утечку всех замороженных в нем ресурсов навсегда**

> **⚠️ `checked continuation` означает, что рантайм будет анализировать (небольшой ценой в производительности), что `continuation` возобновляется один раз в любом случае. Если он возобновляется дважды - приложение крашнется. При отсутствии возобновления рантайм опубликует предупреждение в консоль:** `SWIFT TASK CONTINUATION MISUSE: fetchMessages() leaked its continuation!`

> **⚠️ Вы можете использовать `unsafe continuation`, если _уверены_ в том, что `continuation` будет возобновлен строго один раз, если вопрос производительности стоит на первом плане. Однако, ответственность за непонятное и неадекватное поведение приложения при многократном возобновлении `continuation` ляжет на разработчика.**

Для возобновления `continuation` доступны следующие методы:
- `resume()` - Resumes the suspended task without a value. 
- `resume(returning:)` - Resumes the suspended task and returns the given value. 
- `resume(throwing:)` - Resumes the suspended task, throwing the provided error. 
- `resume(with:)` - Resumes with a Swift.Result. 

Используя `withCheckedContinuation` мы приостановлением текущий код и создаём снапшот остановки, но на этот раз он передается в наше замыкание и мы можем сами запустить его тогда, когда нужно нам.

Примеры конвертирования completion handler в `async`-функцию:

<img src="assets/continuation_completion_handler.png" width="500">
<br>
<img src="assets/continuation_completion_handler_throwing.png" width="600">

Код в примере будет приостановлен в точке `await` до тех пор, пока мы не вызовем `resume()` в запущенном замыкании. 

Дожидаемся отработки метода `fetchMessages` и возобновляем `continuation`. 




<br><br><br>
<a name="continuations_delegates"></a>
## 9. Wrap delegates with continuations

`continuation` - обычный объект с типом `CheckedContinuation<T, Never>` (или `CheckedContinuation<Result<T, Error>, Never>`, или `UnsafeContinuation<T, Never>` и т.д.) и его можно сохранять и передавать куда угодно для дальнейшейго использования. 

Сохраняем `continuation`:

<img src="assets/continuation_wrap_delegate.png" width="580">
<br>

Возобновляем его, когда делегат вернул данные или ошибку:

<img src="assets/continuation_wrap_delegate_usage.png" width="630">


##

↑ [Наверх](#table_of_contents)

##
