# II. AsyncSequence & AsyncStream

<a name="table_of_contents"></a>
## Содержание:

1. [AsyncSequence](#async_sequence)
2. [AsyncStream](#async_stream)
3. [How to manipulate AsyncSequence using map(), filter(), and more](#manipulate_async_sequence)
4. [Converting AsyncSequence to Sequence](#collect_async_sequence)
5. [Custom AsyncSequence](#custom_async_sequence)

← [Назад к Modern Swift Concurrency](../README.md)

##

<a name="async_sequence"></a>
## 1. AsyncSequence

Все знают протокол `Sequence`, который последовательно возвращает значения, пока не вернет `nil`. 
В Swift 5.5 завезли `AsyncSequence` - почти идентичный `Sequence` протокол с важным отличием: каждое значение в нем - асинхронноe.

Примеры асинхронных последовательностей:

<img src="assets/async_sequence_explanation.png" width="580">

<br><br>
Вы можете проходить по `AsyncSequence` в цикле, использовать `break` и `continue`.

Считывание значений из `AsyncSequence` выполняется с помощью `await`:

<img src="assets/async_sequence_example.png" width="340">

На каждой итерации код будет приостановлен до тех пор, пока `AsyncSequence` не вернет следующее значение.




<br><br>
<a name="async_stream"></a>
## 2. AsyncStream

`AsyncStream` - продвинутый `AsyncSequence`, который может возвращать значения гораздо быстрее, чем программа может успевать их обрабатывать. В этом случае необходима реализация механизмов буфферизации значений или их пропуска.




<br><br><br>
<a name="manipulate_async_sequence"></a>
## 3. How to manipulate AsyncSequence using map(), filter(), and more

Для `AsyncSequence` доступны почти все те же функции, что и для `Sequence`: `map`, `filter`, `reduce`, `first`, `prefix` и т.д. Но они отличаются друг от друга по способу ожидания результата.

Такие функции, как `reduce`, `allSatisfy`, `min`, `max` и другие, которым для работы необходима _вся_ коллекция, дождутся _полной_ загрузки всех элементов `AsyncSequence`, только после чего вернут результат. 

<img src="assets/async_sequence_all_satisfy.png" width="630">

> **⚠️ Если `AsyncSequence` бесконечна, то функции, которым для работы нужна вся коллекция, никогда не вернут результат!**

`map`, `prefix`, `filter` и им подобные могут обрабатывать элементы _поштучно_ по мере их получения.

Функции _отличаются_ от классических возвращаемым результатом.

`map`, `filter`, `reduce` и т.д., применённые на `AsyncSequence`, возвращают `AsyncSequence` нового типа, а фактически модификации будут применены в момент обращения к элементам. Похоже на `LazySequence`.

<img src="assets/manipulate_async_sequence.png" width="680">

- `map()` -> `AsyncMapSequence`
- `filter()` -> `AsyncFilterSequence`
- `map().filter().prefix()` -> `AsyncMapFilterPrefixSequence`

При последовательном применении функций высшего порядка пригодится `some`. `some AsyncSequnce` - «какой-то из» `AsyncSequnce`. Но какой-то один _конкретный_, который вычислит компилятор при сборке. А не любой тип, реализующий `AsyncSequnce`. 

В примере выше тип возвращаемого значения будет вычислен компилятором как `AsyncFilterPrefixMapSequence`.




<br><br><br>
<a name="collect_async_sequence"></a>
## 4. Converting AsyncSequence to Sequence

С помощью функции `reduce` можно превратить `AsyncSequence` в обычную `Sequence`, _дождавшись_ всех элементов:

<img src="assets/async_sequence_collect.png" width="600">




<br><br><br>
<a name="custom_async_sequence"></a>
## 5. Custom AsyncSequence

Чтобы написать собственную `AsyncSequence`, нужно реализовать протокол `AsyncIteratorProtocol` и два метода: `next() async` и `makeAsyncIterator()`:

<img src="assets/custom_async_sequence.png" width="630">


##

↑ [Наверх](#table_of_contents)

##
